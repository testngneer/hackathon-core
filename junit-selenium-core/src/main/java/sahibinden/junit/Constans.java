package sahibinden.junit;

public interface Constans {


   interface Env {

      String SELENIUM_PROFILE = "SELENIUM_PROFILE";

      String GALEN_SPEC_FOLDER = "galen.specFolderPath";

      String SCREENSHOTS_FOLDER = "reports.screenshotPath";

      String SELENIUM_DELAY = "SELENIUM_DELAY";

      String TAKE_SCREENSHOT = "TAKE_SCREENSHOT";

   }

   interface MandatoryBrowserProperties {

      String[] CHROME = {};

   }

}
