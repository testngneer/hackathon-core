package sahibinden.junit.reflect;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.RemoteWebDriver;
import sahibinden.junit.Constans;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.WebDriverCustom;
import sahibinden.junit.exception.SeleniumJUnitException;

import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

public final class DriverInvocationHandler implements InvocationHandler {

   private final static Logger logger = Logger.getLogger(DriverInvocationHandler.class.getName());

   private final RemoteWebDriver remoteWebDriver;
   private final WebDriverCustom webDriverCustom;
   private final SeleniumSession seleniumSession;
   private String screenShotPath;
   private int delay = 1;
   private boolean takeScreenshot = false;

   public DriverInvocationHandler(RemoteWebDriver remoteWebDriver, WebDriverCustom webDriverCustom, SeleniumSession seleniumSession) {
      this.remoteWebDriver = remoteWebDriver;
      this.webDriverCustom = webDriverCustom;
      this.seleniumSession = seleniumSession;
      this.init();
   }

   public void init() {
      String delay = System.getProperty(Constans.Env.SELENIUM_DELAY);
      String takeScreenShot = System.getProperty(Constans.Env.TAKE_SCREENSHOT);
      if (takeScreenShot != null) {
         try {
            this.takeScreenshot = Boolean.parseBoolean(takeScreenShot);
            if (this.takeScreenshot) {
               this.screenShotPath = seleniumSession.getProperty(Constans.Env.SCREENSHOTS_FOLDER);
            }
         } catch (Exception e) {

         }
      }
      if (delay != null) {
         try {
            this.delay = Integer.parseInt(delay);
         } catch (Exception e) {

         }
      }
   }

   @Override
   public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      ensureForDelay();
      if (method.equals(DriverMetaData.WEB_DRIVER_GET_METHOD)) {
         try {
            Object o = method.invoke(remoteWebDriver, args);
            webDriverCustom.initSeleniumHelper();
            return o;
         } catch (Exception e) {
            throw new SeleniumJUnitException("Execution Failed", e);
         } finally {
            takeScreenShot();
         }
      } else {
         int tryLimit = 3;
         int tryCount = 0;

         while (true) {
            try {
               return tryToExecuteMethod(method, args);
            } catch (Exception e) {
               tryCount++;
               if (tryLimit == tryCount) {
                  throw new SeleniumJUnitException("Execution failed", e);
               }
               Thread.sleep(5000);
            }
         }
      }
   }

   private Object tryToExecuteMethod(Method method, Object[] args) throws Exception {
      try {
         return method.invoke(remoteWebDriver, args);
      } catch (IllegalArgumentException e) {
         return method.invoke(webDriverCustom, args);
      } catch (Exception e) {
         throw new SeleniumJUnitException("Execution Failed", e);
      } finally {
         takeScreenShot();
      }
   }

   private void ensureForDelay() throws Exception {
      if (delay != 0) {
         Thread.sleep(1000 * delay);
      }
   }

   private void takeScreenShot() {
      if (this.takeScreenshot) {
         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmSS");
         String newPath = screenShotPath + File.separator + this.seleniumSession.getTestName() + "_" + simpleDateFormat.format(seleniumSession.getSessionCreatedDate());
         File file = this.remoteWebDriver.getScreenshotAs(OutputType.FILE);
         File Path = new File(newPath);
         Path.mkdirs();
         File newFile = new File(newPath + "/" + simpleDateFormat.format(new Date()) + "_" + file.getName());
         try {
            FileUtils.copyFile(file, newFile);
            logger.info("taking screenshot %s : " + newFile.getPath());
         } catch (Exception e) {

         }
      }
   }
}
