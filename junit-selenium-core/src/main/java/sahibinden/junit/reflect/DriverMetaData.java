package sahibinden.junit.reflect;

import org.openqa.selenium.WebDriver;
import sahibinden.junit.exception.SeleniumJUnitRuntimeException;

import java.lang.reflect.Method;

public class DriverMetaData {

   public final static Class<WebDriver> WEB_DRIVER_CLASS = WebDriver.class;

   public final static Method WEB_DRIVER_GET_METHOD;

   static {
      try {
         WEB_DRIVER_GET_METHOD = WEB_DRIVER_CLASS.getMethod("get", String.class);
      } catch (Exception e) {
         throw new SeleniumJUnitRuntimeException(e);
      }

   }
}
