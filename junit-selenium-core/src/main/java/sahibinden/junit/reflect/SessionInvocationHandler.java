package sahibinden.junit.reflect;


import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.Browser;
import sahibinden.junit.core.SeleniumThreadFactory;
import sahibinden.junit.exception.SeleniumJUnitException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class SessionInvocationHandler implements InvocationHandler {

   private final SeleniumSession seleniumSession;
   private final ExecutorService executorService;
   private final ThreadFactory threadFactory;

   public SessionInvocationHandler(SeleniumSession seleniumSession) {
      this.seleniumSession = seleniumSession;
      this.threadFactory = new SeleniumThreadFactory(seleniumSession);
      this.executorService = Executors.newFixedThreadPool(4, threadFactory);
   }

   @Override
   public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      List<SeleniumSession.DriverWrapper> browserList = seleniumSession.getDrivers();
      List<Object> results = new ArrayList<>();
      List<SessionInvocationResult> invocationResults = new ArrayList<>();

      for (SeleniumSession.DriverWrapper each : browserList) {
         SessionInvocationResult result = new SessionInvocationResult(method, each.getBrowser(), args);
         invocationResults.add(result);
      }

      List<Future<SessionInvocationResult>> futures = this.executorService.invokeAll(invocationResults);

      for (Future<SessionInvocationResult> each : futures) {
         SessionInvocationResult sessionInvocationResult = each.get();

         if (sessionInvocationResult.e != null) {
            throw new SeleniumJUnitException("invocation failed", sessionInvocationResult.e);
         }
         if (sessionInvocationResult.result != null) {
            results.add(sessionInvocationResult.result);
         }
      }

      return results.size() > 0 ? results.get(0) : null;
   }

   public class SessionInvocationResult implements Callable<SessionInvocationResult> {

      private final Method method;
      private final Browser browser;
      private final Object[] args;
      private Exception e;
      private Object result;

      private SessionInvocationResult(Method method, Browser browser, Object[] args) {
         this.method = method;
         this.browser = browser;
         this.args = args;
      }

      @Override
      public SessionInvocationResult call() throws Exception {
         try {
            result = method.invoke(browser, args);
         } catch (Exception e) {
            this.e = e;
         } finally {
            return this;
         }
      }
   }
}
