package sahibinden.junit;

import net.lightbody.bmp.BrowserMobProxy;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.*;
import org.yaml.snakeyaml.Yaml;
import sahibinden.junit.api.*;
import sahibinden.junit.core.DriverAnnotateWrapper;
import sahibinden.junit.core.GalenWrapper;
import sahibinden.junit.exception.SeleniumJUnitException;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Logger;

public class SeleniumExtension implements BeforeEachCallback, AfterEachCallback, ParameterResolver, BeforeAllCallback, AfterAllCallback {

   private static final Logger logger = Logger.getLogger(SeleniumExtension.class.getName());

   private static final ExtensionContext.Namespace EXTENSION_NAMESPACE = ExtensionContext.Namespace.create(new Object[]{"com.sahibinden.junit"});

   private static final Class<SeleniumTestApplication> SELENIUM_TEST_APPLICATION_CLASS = SeleniumTestApplication.class;

   private static final Class<PageObject> PAGE_OBJECT_CLASS = PageObject.class;

   private static final Class<Driver> DRIVER_CLASS = Driver.class;

   private static final Class<Drivers> DRIVERS_CLASS = Drivers.class;

   private static final Class<Browser> BROWSER_API_CLASS = Browser.class;

   private static final Class<DisplayName> DISPLAY_NAME_CLASS = DisplayName.class;

   private static final Class<SeleniumSession> SESSION_CLASS = SeleniumSession.class;

   private static final Class<GalenApi> GALEN_TEST = GalenApi.class;

   private static final Class<BrowserMobProxy> PROXY_CLASS = BrowserMobProxy.class;

   private static final String SESSION = "session";

   private static final String SELENIUM_HELPER_JS_PATH = "js/seleniumHelper.js";

   public static String SELENIUM_HELPER_JS;

   private Properties properties;


   static {
      try {
         readSeleniumHelper();
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
   }

   @Override
   public void afterEach(ExtensionContext context) throws Exception {
      ExtensionContext.Store store = context.getStore(EXTENSION_NAMESPACE);
      SeleniumSession seleniumSession = store.get(SESSION, SeleniumSession.class);
      seleniumSession.destroySession();
   }

   @Override
   public void beforeEach(ExtensionContext context) throws Exception {
      DisplayName displayName = context.getElement().get().getAnnotation(DISPLAY_NAME_CLASS);
      ExtensionContext.Store store = context.getStore(EXTENSION_NAMESPACE);
      Object testInstance = context.getRequiredTestInstance();
      List<DriverAnnotateWrapper> driver = findDriversFromContext(context);
      SeleniumSession seleniumSession = createSeleniumSession(testInstance, driver);
      seleniumSession.initSession();
      if (displayName != null) {
         seleniumSession.setTestName(displayName.value());
      }
      checkTestInstanceForGlobalBrowserVariables(testInstance, seleniumSession);
      store.put(SESSION, seleniumSession);
   }

   @Override
   public void afterAll(ExtensionContext context) throws Exception {

   }

   @Override
   public void beforeAll(ExtensionContext context) throws Exception {
      AnnotatedElement annotatedElement = context.getElement().get();
      if (!isSeleniumApplication(annotatedElement)) {
         throw new SeleniumJUnitException("Test class must be annotated with @SeleniumTestApplication annotation");
      }
      SeleniumTestApplication app = annotatedElement.getAnnotation(SELENIUM_TEST_APPLICATION_CLASS);
      parseConfigurations(app);
      loadEnvProperties();
   }

   @Override
   public boolean supportsParameter(
           ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {

      if (parameterContext.getParameter().getType().equals(BROWSER_API_CLASS)) {
         return true;
      }

      if (parameterContext.getParameter().isAnnotationPresent(PAGE_OBJECT_CLASS)) {
         return true;
      }

      if (parameterContext.getParameter().getType().equals(SESSION_CLASS)) {
         return true;
      }

      if (parameterContext.getParameter().getType().equals(GALEN_TEST)) {
         return true;
      }

      if (parameterContext.getParameter().getType().equals(PROXY_CLASS)) {
         return true;
      }


      return false;
   }

   @Override
   public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
      ExtensionContext.Store store = extensionContext.getStore(EXTENSION_NAMESPACE);
      SeleniumSession seleniumSession = store.get(SESSION, SeleniumSession.class);
      Optional<Driver> driver = Optional.ofNullable(parameterContext.getParameter().getAnnotation(DRIVER_CLASS));

      if (parameterContext.getParameter().getType().equals(BROWSER_API_CLASS)) {
         try {
            return seleniumSession.getSuitableBrowser(driver);
         } catch (Exception e) {
            throw new ParameterResolutionException("Parameter Resolution Failed for driver", e);
         }
      }

      if (parameterContext.getParameter().isAnnotationPresent(PAGE_OBJECT_CLASS)) {
         try {
            return seleniumSession.initAndReturnPageObject(parameterContext.getParameter().getType(), driver);
         } catch (Exception e) {
            throw new ParameterResolutionException("Page object resolve failed", e);
         }
      }

      if (parameterContext.getParameter().getType().equals(GALEN_TEST)) {
         try {
            Browser browser = seleniumSession.getSuitableBrowser(driver);
            return new GalenWrapper(browser, seleniumSession);
         } catch (Exception e) {
            throw new ParameterResolutionException("Parameter Resolution Failed for driver", e);
         }
      }

      if (parameterContext.getParameter().getType().equals(PROXY_CLASS)) {
         try {
            return seleniumSession.getProxy();
         } catch (Exception e) {
            throw new ParameterResolutionException("Parameter Resolution failed for proxy", e);
         }
      }

      if (parameterContext.getParameter().getType().equals(SESSION_CLASS)) {
         return seleniumSession;
      }

      return null;
   }

   private List<DriverAnnotateWrapper> findDriversFromContext(ExtensionContext extensionContext) throws Exception {
      List<DriverAnnotateWrapper> driverList = new ArrayList<>();
      String browser = System.getProperty("BROWSER");

      AnnotatedElement currentContext = extensionContext.getElement().get();
      AnnotatedElement parent = extensionContext.getParent().get().getElement().get();
      if (currentContext.isAnnotationPresent(DRIVERS_CLASS)) {
//         Drivers drivers = currentContext.getAnnotation(DRIVERS_CLASS);
//         driverList.addAll(Arrays.asList(drivers.drivers()));
      } else if (currentContext.isAnnotationPresent(DRIVER_CLASS)) {
         Driver driver = currentContext.getAnnotation(DRIVER_CLASS);
         DriverAnnotateWrapper driverWrapper = DriverAnnotateWrapper.createFromDriver(driver);
         if (browser != null) {
            logger.info(String.format("browser found from jenkins %s", browser));
            driverWrapper.setDriver(DriverType.valueOf(browser));
         }
         driverList.add(driverWrapper);

      } else if (parent.isAnnotationPresent(DRIVERS_CLASS)) {
//         Drivers drivers = parent.getAnnotation(DRIVERS_CLASS);
//         driverList.addAll(Arrays.asList(drivers.drivers()));
      } else if (parent.isAnnotationPresent(DRIVER_CLASS)) {
         Driver driver = parent.getAnnotation(DRIVER_CLASS);
         DriverAnnotateWrapper driverWrapper = DriverAnnotateWrapper.createFromDriver(driver);
         if (browser != null) {
            logger.info(String.format("browser found from jenkins %s", browser));
            driverWrapper.setDriver(DriverType.valueOf(browser));
         }
         driverList.add(driverWrapper);
      }
      if (driverList.isEmpty()) {
         throw new SeleniumJUnitException("No driver found for selenium test");
      }
      return driverList;

   }

   private boolean isSeleniumApplication(AnnotatedElement annotatedElement) {
      return annotatedElement.isAnnotationPresent(SELENIUM_TEST_APPLICATION_CLASS);
   }

   private void parseConfigurations(SeleniumTestApplication seleniumTestApplication) throws Exception {
      String propertiesFile = seleniumTestApplication.propertiesFile();
      String profile = System.getProperty(Constans.Env.SELENIUM_PROFILE);

      if (profile == null) {
         propertiesFile = propertiesFile.replace("-" + SeleniumTestApplication.PROFILE_SUFFIX, "");
      } else {
         propertiesFile = propertiesFile.replace("-" + SeleniumTestApplication.PROFILE_SUFFIX, "-" + profile);
      }
      logger.info(String.format("parsing properties file %s", propertiesFile));
      parseDefaultsConfigurations(propertiesFile);
   }

   private void parseDefaultsConfigurations(String propertiesFile) throws Exception {
      InputStream inputStream;
      try {

         inputStream = this.getClass().getClassLoader().getResourceAsStream(propertiesFile.replace(SeleniumTestApplication.PROPERTIES_SUFFIX, "yml"));
         parseYmlFileType(inputStream);
      } catch (Exception e) {
         inputStream = this.getClass().getClassLoader().getResourceAsStream(propertiesFile.replace(SeleniumTestApplication.PROPERTIES_SUFFIX, "properties"));
         parsePropertiesFileType(inputStream);
      }
      if (inputStream == null) {
         System.out.println("failed configuration");
      }
   }


   private SeleniumSession createSeleniumSession(Object testInstance, List<DriverAnnotateWrapper> driver) {
      SeleniumSession seleniumSession = new DefaultSeleniumSession(testInstance, driver, properties);
      return seleniumSession;
   }

   private void parseYmlFileType(InputStream inputStream) throws Exception {
      Yaml yaml = new Yaml();
      Map map = yaml.loadAs(inputStream, Map.class);
      Properties properties = new Properties();
      convertMapToProperties("", map, properties);
      this.properties = properties;
   }

   private void convertMapToProperties(String prefix, Map map, Properties properties) {
      String editedPrefix = prefix == "" ? prefix : prefix + ".";
      for (Object key : map.keySet()) {
         Object value = map.get(key);
         if (value.getClass().isAssignableFrom(LinkedHashMap.class)) {
            convertMapToProperties(editedPrefix + String.valueOf(key), (Map) value, properties);
         } else {
            properties.put(editedPrefix + String.valueOf(key), value);
         }
      }
   }

   private void checkTestInstanceForGlobalBrowserVariables(Object obj,
                                                           SeleniumSession seleniumSession) throws Exception {
      Class currentClass = obj.getClass();
      do {
         Field[] fields = currentClass.getDeclaredFields();
         injectFieldValues(fields, obj, seleniumSession);
         currentClass = currentClass.getSuperclass();
      } while (!currentClass.equals(Object.class));
   }

   private void injectFieldValues(Field[] fields, Object sourceObject, SeleniumSession seleniumSession) throws Exception {
      for (Field each : fields) {

         if (each.isAnnotationPresent(DRIVER_CLASS)) {
            Optional<Driver> driver = Optional.ofNullable(each.getAnnotation(Driver.class));
            Browser browser = seleniumSession.getSuitableBrowser(driver);
            each.setAccessible(true);
            each.set(sourceObject, browser);
         }

         if (each.isAnnotationPresent(PAGE_OBJECT_CLASS)) {
            Object o
                    = seleniumSession.initAndReturnPageObject(each.getType(), Optional.ofNullable(null));
            each.setAccessible(true);
            each.set(sourceObject, o);
         }
      }
   }

   private void parsePropertiesFileType(InputStream inputStream) throws Exception {
      Properties properties = new Properties();
      properties.load(inputStream);
      this.properties = properties;
   }

   private void loadEnvProperties() {
      for (Object each : properties.keySet()) {
         if (System.getProperty(String.valueOf(each)) == null)
            System.setProperty(String.valueOf(each), String.valueOf(properties.get(each)));
      }
   }

   private static void readSeleniumHelper() throws Exception {
      InputStream inputStream = SeleniumExtension.class.getClassLoader().getResourceAsStream(SELENIUM_HELPER_JS_PATH);
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      int nRead;
      byte[] data = new byte[1024];
      while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
         buffer.write(data, 0, nRead);
      }
      inputStream.close();
      SELENIUM_HELPER_JS = new String(buffer.toByteArray(), Charset.forName("UTF-8"));
   }


}
