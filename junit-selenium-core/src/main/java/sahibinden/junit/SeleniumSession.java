package sahibinden.junit;

import net.lightbody.bmp.BrowserMobProxy;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import sahibinden.junit.api.Browser;
import sahibinden.junit.api.Driver;
import sahibinden.junit.api.DriverType;
import sahibinden.junit.api.WebDriverCustom;
import sahibinden.junit.core.DriverAnnotateWrapper;
import sahibinden.junit.core.WebDriverCustomImpl;
import sahibinden.junit.exception.SeleniumJUnitException;
import sahibinden.junit.exception.SeleniumJUnitRuntimeException;
import sahibinden.junit.reflect.DriverInvocationHandler;
import sahibinden.junit.reflect.SessionInvocationHandler;

import java.lang.reflect.Proxy;
import java.util.*;

public abstract class SeleniumSession {

   private final Object testInstance;
   private final List<DriverAnnotateWrapper> driverProperties;
   private final Properties envProperties;
   private final List<DriverWrapper> drivers = new ArrayList<>();
   private String testName;
   private Browser browser;
   private BrowserMobProxy proxy;
   private final Date sessionCreatedDate;

   protected SeleniumSession(Object testInstance, List<DriverAnnotateWrapper> driverProperties, Properties envProperties) {
      this.testInstance = testInstance;
      this.driverProperties = driverProperties;
      this.envProperties = envProperties;
      this.sessionCreatedDate = new Date();
   }

   protected void initSession() throws Exception {
      this.initSessionProxy();
      this.initDriverProxies();
   }

   protected void destroySession() throws Exception {
      try {
         browser.close();
      } catch (Exception e) {

      }
      try {
         browser.quit();
      } catch (Exception e) {

      }
   }

   protected Properties getEnvProperties() {
      return envProperties;
   }

   public String getProperty(String key) {
      return this.getProperty(key, String.class);
   }

   public <T> T getProperty(String key, Class<T> aClass) {
      Object o = this.envProperties.get(key);
      if (o == null) {
         throw new SeleniumJUnitRuntimeException(String.format("key \'%s\' property must be set ", key));
      }
      if (o.getClass().equals(aClass)) {
         return (T) o;
      }
      throw new RuntimeException(" not suited class for given ");
   }

   protected abstract RemoteWebDriver createDriver(DriverAnnotateWrapper driver) throws Exception;

   private void initSessionProxy() throws Exception {
      this.browser = (Browser)
              Proxy.newProxyInstance(Browser.class.getClassLoader(),
                      Browser.CLASS, new SessionInvocationHandler(this));
   }

   private void initDriverProxies() throws Exception {
      for (DriverAnnotateWrapper each : driverProperties) {
         try {
            RemoteWebDriver remoteWebDriver = createDriver(each);
            WebDriverCustom webDriverCustom = new WebDriverCustomImpl(remoteWebDriver, this);
            Browser o = (Browser)
                    Proxy.newProxyInstance(Browser.class.getClassLoader(),
                            Browser.CLASS, new DriverInvocationHandler(remoteWebDriver, webDriverCustom, this));
            drivers.add(new DriverWrapper(each, o));
         } catch (Exception e) {
            throw e;
         }
      }
   }

   public List<DriverWrapper> getDrivers() {
      return drivers;
   }

   public Browser getSuitableBrowser(Optional<Driver> driver) throws Exception {
      if (drivers.size() == 0) {
         throw new SeleniumJUnitException("no driver found");
      }

      if (drivers.size() == 1) {
         return drivers.get(0).getBrowser();
      }

      if (!driver.isPresent())
         return this.browser;

      DriverType driverType = driver.get().driver();

      Optional<DriverWrapper> browser = this.drivers.stream()
              .filter(x -> x.getDriver().getDriver().equals(driverType)).findFirst();

      if (!browser.isPresent()) {
         throw new SeleniumJUnitException("no suitable driver");
      }

      return browser.get().getBrowser();
   }

   public <T> T initAndReturnPageObject(Class<T> pageObjectClass,
                                        Optional<Driver> driver) throws Exception {

      if (drivers.size() == 0) {
         throw new SeleniumJUnitException("no driver found");
      }

      if (drivers.size() == 1) {
         T pageInstance = pageObjectClass.newInstance();
         PageFactory.initElements(drivers.get(0).getBrowser(), pageInstance);
         return pageInstance;
      }

      if (!driver.isPresent()) {
         throw new SeleniumJUnitException("multiple driver found , driver must be provided");
      }

      Browser browser = this.getSuitableBrowser(driver);

      if (browser == null) {
         throw new SeleniumJUnitException("no suitable driver found for page object");
      }
      T pageInstance = pageObjectClass.newInstance();
      PageFactory.initElements(browser, pageInstance);
      return pageInstance;
   }

   public final class DriverWrapper {
      private final DriverAnnotateWrapper driver;
      private final Browser browser;

      private DriverWrapper(DriverAnnotateWrapper driver, Browser browser) {
         this.driver = driver;
         this.browser = browser;
      }

      public DriverAnnotateWrapper getDriver() {
         return driver;
      }

      public Browser getBrowser() {
         return browser;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         DriverWrapper that = (DriverWrapper) o;
         return Objects.equals(driver, that.driver);
      }

      @Override
      public int hashCode() {

         return Objects.hash(driver);
      }
   }

   public BrowserMobProxy getProxy() throws Exception {
      if (proxy == null) {
         throw new SeleniumJUnitException("Proxy not created wtf are you doing");
      }
      return proxy;
   }

   public String getTestName() {
      return testName;
   }

   public void setTestName(String testName) {
      this.testName = testName;
   }

   public void setProxy(BrowserMobProxy proxy) {
      this.proxy = proxy;
   }

   public Date getSessionCreatedDate() {
      return sessionCreatedDate;
   }


}
