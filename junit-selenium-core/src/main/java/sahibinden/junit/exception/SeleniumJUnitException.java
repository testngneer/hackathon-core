package sahibinden.junit.exception;

public class SeleniumJUnitException extends Exception
{
    public SeleniumJUnitException()
    {
    }

    public SeleniumJUnitException(String message)
    {
        super(message);
    }

    public SeleniumJUnitException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public SeleniumJUnitException(Throwable cause)
    {
        super(cause);
    }

    public SeleniumJUnitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
