package sahibinden.junit.dto;

public class SeleniumRequestDto {

   private Long id;
   private String response;
   private String responseText;
   private int status;
   private String responseUrl;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getResponse() {
      return response;
   }

   public void setResponse(String response) {
      this.response = response;
   }

   public String getResponseText() {
      return responseText;
   }

   public void setResponseText(String responseText) {
      this.responseText = responseText;
   }

   public int getStatus() {
      return status;
   }

   public void setStatus(int status) {
      this.status = status;
   }

   public String getResponseUrl() {
      return responseUrl;
   }

   public void setResponseUrl(String responseUrl) {
      this.responseUrl = responseUrl;
   }
}
