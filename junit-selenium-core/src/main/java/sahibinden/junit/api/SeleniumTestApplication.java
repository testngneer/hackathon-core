package sahibinden.junit.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SeleniumTestApplication {

   String PROPERTIES_SUFFIX = "${suffix}";

   String PROFILE_SUFFIX = "${profile}";

   String DEFAULT_PROPERTIES_FILE_PATH = "selenium-application-" + PROFILE_SUFFIX + "." + PROPERTIES_SUFFIX;

   String propertiesFile() default DEFAULT_PROPERTIES_FILE_PATH;

}
