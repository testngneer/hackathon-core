package sahibinden.junit.api;

import org.openqa.selenium.WebElement;
import sahibinden.junit.dto.SeleniumRequestDto;

import java.util.List;

public interface WebDriverCustom {

   void initSeleniumHelper();

   long getActiveRequestCount();

   void click(WebElement webElement);

   void submit(WebElement webElement);

   void sendKeys(WebElement webElement, CharSequence... seq);

   List<SeleniumRequestDto> getAllFinishedRequests();

   SeleniumRequestDto findFinishedRequestById(int id);

}
