package sahibinden.junit.api;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public interface Browser extends WebDriver, JavascriptExecutor, TakesScreenshot,WebDriverCustom {

   Class[] CLASS = new Class[]{Browser.class};



}