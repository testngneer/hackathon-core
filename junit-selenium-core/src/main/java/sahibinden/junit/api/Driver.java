package sahibinden.junit.api;

import sahibinden.junit.core.Agents;
import sahibinden.junit.factory.driver.DefaultDriverFactory;
import sahibinden.junit.factory.driver.WebDriverFactory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Driver {

   DriverType driver() default DriverType.CHROME;

   String mobileAgent() default Agents.NONE;

   Class<? extends WebDriverFactory> driverFactory() default DefaultDriverFactory.class;

   int width() default 0;

   int heigth() default 0;

   boolean fullScreen() default false;

   double pixelRatio() default 3.0;

   boolean proxy() default false;

   int proxyPort() default 9292;

}
