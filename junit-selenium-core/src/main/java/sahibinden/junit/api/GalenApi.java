package sahibinden.junit.api;

import java.io.IOException;
import java.util.List;

public interface GalenApi {

   void checkLayoutDesign(String specFile, List<String> includedTags, String className) throws IOException;


}
