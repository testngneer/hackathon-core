package sahibinden.junit.factory.driver;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.Driver;
import sahibinden.junit.core.Agents;
import sahibinden.junit.core.DriverAnnotateWrapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class DefaultDriverFactory extends WebDriverFactory {

   public DefaultDriverFactory(SeleniumSession seleniumSession) {
      super(seleniumSession);
   }

   private RemoteWebDriver createWebDriverForChrome(Properties properties, Driver driver) throws Exception {
      DesiredCapabilities capabilities = new DesiredCapabilities().chrome();
      ChromeOptions chromeOptions = new ChromeOptions();
      chromeOptions.merge(capabilities);
      if (driver.proxy()) {
         BrowserMobProxy browserMobProxy = getProxyServer(driver.proxyPort());
         Proxy proxy = ClientUtil.createSeleniumProxy(browserMobProxy);
         seleniumSession.setProxy(browserMobProxy);
         chromeOptions.setProxy(proxy);
      }
      ChromeDriver chromeDriver = new ChromeDriver(chromeOptions);
      if (driver.fullScreen()) {
         chromeDriver.manage().window().fullscreen();
      } else if (driver.width() != 0 && driver.heigth() != 0) {
         chromeDriver.manage().window().setSize(new Dimension(driver.width(), driver.heigth()));
      }
      return chromeDriver;
   }


   public BrowserMobProxy getProxyServer(int port) {
      BrowserMobProxy proxy = new BrowserMobProxyServer();
      proxy.setTrustAllServers(true);
      proxy.start(port);
      return proxy;
   }

   private RemoteWebDriver createWebDriverForMobileChrome(Properties properties, Driver driver) {
      DesiredCapabilities capabilities = new DesiredCapabilities().chrome();

      Map<String, Object> deviceMetrics = new HashMap<>();
      deviceMetrics.put("width", driver.width());
      deviceMetrics.put("height", driver.heigth());
      deviceMetrics.put("pixelRatio", driver.pixelRatio());
      Map<String, Object> mobileEmulation = new HashMap<>();

      mobileEmulation.put("deviceMetrics", deviceMetrics);
      mobileEmulation.put("userAgent", driver.mobileAgent());


      Map<String, Object> chromeOptions = new HashMap<>();
      chromeOptions.put("mobileEmulation", mobileEmulation);


      capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

      ChromeDriver chromeDriver = new ChromeDriver(capabilities);
      chromeDriver.manage().window().setSize(new Dimension(driver.width(), driver.heigth()));


      return chromeDriver;
   }


   @Override
   public RemoteWebDriver createDriver(Properties properties, DriverAnnotateWrapper driver) throws Exception {
      switch (driver.getDriver()) {
         case CHROME:
            if(driver.getMobileAgent().equals(Agents.NONE)) {
               return createWebDriverForChrome(properties, driver.getAnnotate());
            }else{
               return createWebDriverForMobileChrome(properties, driver.getAnnotate());
            }
         case FIREFOX:
            return new FirefoxDriver();
         case SAFARI:
            return new SafariDriver();
         case OPERA:
            return new OperaDriver();
         default:
            throw new Exception("");
      }
   }
}
