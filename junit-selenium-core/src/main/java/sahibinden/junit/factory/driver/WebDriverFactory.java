package sahibinden.junit.factory.driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.Driver;
import sahibinden.junit.core.DriverAnnotateWrapper;

import java.util.Properties;

public abstract class WebDriverFactory {

   protected SeleniumSession seleniumSession;

   public WebDriverFactory(SeleniumSession seleniumSession) {
      this.seleniumSession = seleniumSession;
   }

   public abstract RemoteWebDriver createDriver(Properties properties, DriverAnnotateWrapper driver) throws Exception;

}
