package sahibinden.junit;

import org.openqa.selenium.remote.RemoteWebDriver;
import sahibinden.junit.core.DriverAnnotateWrapper;
import sahibinden.junit.factory.driver.WebDriverFactory;

import java.util.List;
import java.util.Properties;

public class DefaultSeleniumSession extends SeleniumSession {

   protected DefaultSeleniumSession(Object testInstance, List<DriverAnnotateWrapper> driverProperties, Properties envProperties) {
      super(testInstance, driverProperties, envProperties);
   }

   @Override
   protected RemoteWebDriver createDriver(DriverAnnotateWrapper driver) throws Exception {
      WebDriverFactory webDriverFactory = driver.getDriverFactory().getConstructor(SeleniumSession.class).newInstance(this);
      RemoteWebDriver webDriver = webDriverFactory.createDriver(getEnvProperties(), driver);
      return webDriver;
   }

}
