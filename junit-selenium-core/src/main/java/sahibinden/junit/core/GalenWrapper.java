package sahibinden.junit.core;

import com.galenframework.api.Galen;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import com.galenframework.reports.model.LayoutReport;
import org.junit.jupiter.api.Assertions;
import org.junit.platform.commons.util.StringUtils;
import sahibinden.junit.Constans;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.Browser;
import sahibinden.junit.api.GalenApi;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class GalenWrapper implements GalenApi {

   private Logger logger = Logger.getLogger(GalenWrapper.class.getName());

   private final Browser browser;
   private final SeleniumSession seleniumSession;

   private String filePathRoot;
   private String fullFilePath;
   private String reportFilePath;

   public GalenWrapper(Browser browser, SeleniumSession seleniumSession) {
      this.browser = browser;
      this.seleniumSession = seleniumSession;
   }

   @Override
   public void checkLayoutDesign(String specFile,
                                 List<String> includedTags,
                                 String className) throws IOException {

      String pathPrefix;

      specFile = "" + specFile;

      defineTargetGalenReportDirectory(className);

      LayoutReport layoutReport = Galen.checkLayout(browser, specFile, includedTags);

      // Creating a list of tests
      List<GalenTestInfo> tests = new LinkedList<GalenTestInfo>();

      // Creating an object that will contain the information about the test
      GalenTestInfo test = GalenTestInfo.fromString(className);

      // Adding layout report to the test report
      test.getReport().layout(layoutReport, "check layout on desktop");
      tests.add(test);

      // Exporting all test reports to html
      new HtmlReportBuilder().build(tests, reportFilePath);

      if (layoutReport.errors() > 0) {
         Assertions.fail("Incorrect layout: " + specFile);
      }
   }

   private void defineTargetGalenReportDirectory(String className) {
      File dir;

      //Define a target directory for failed test result artifacts
      if (!StringUtils.isBlank(System.getenv("WORKSPACE"))) {
         dir = new File(System.getenv("WORKSPACE") + File.separator + "GalenReports");
      } else {
         dir = new File(System.getProperty("user.home") + File.separator + "GalenReports");
      }

      //Create directory if does not exist
      if (dir.exists()) {
         logger.info("A folder with name 'GalenReports' is already exist in the path ");
      } else {
         dir.mkdirs();
      }

      //With the "\\" attributes of path a hierarchical folder structure is automatically build when file save
      filePathRoot = dir.toString();
      fullFilePath = filePathRoot + System.getProperty("file.separator") + className;

      new File(fullFilePath).mkdirs();

      DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
      Date date = new Date();

      reportFilePath = fullFilePath + System.getProperty("file.separator") + dateFormat.format(date);
      logger.info("Galen report file : " + reportFilePath);
   }
}
