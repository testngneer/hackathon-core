package sahibinden.junit.core;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import sahibinden.junit.SeleniumExtension;
import sahibinden.junit.SeleniumSession;
import sahibinden.junit.api.WebDriverCustom;
import sahibinden.junit.dto.SeleniumRequestDto;
import sahibinden.junit.exception.SeleniumJUnitRuntimeException;

import java.util.Collections;
import java.util.List;

public class WebDriverCustomImpl implements WebDriverCustom {

   private final RemoteWebDriver remoteWebDriver;
   private final SeleniumSession seleniumSession;
   private final ObjectMapper objectMapper;

   public WebDriverCustomImpl(RemoteWebDriver remoteWebDriver, SeleniumSession seleniumSession) {
      this.remoteWebDriver = remoteWebDriver;
      this.seleniumSession = seleniumSession;
      this.objectMapper = new CustomObjectMapper();
   }

   public WebDriverCustomImpl(RemoteWebDriver remoteWebDriver, SeleniumSession seleniumSession, ObjectMapper objectMapper) {
      this.remoteWebDriver = remoteWebDriver;
      this.seleniumSession = seleniumSession;
      this.objectMapper = objectMapper;
   }

   @Override
   public void initSeleniumHelper() {
      this.remoteWebDriver.executeScript(SeleniumExtension.SELENIUM_HELPER_JS);
      try {
         Thread.sleep(1000 * 5);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
   }

   @Override
   public long getActiveRequestCount() {
      try {
         Object o = this.remoteWebDriver
                 .executeScript("return window.__seleniumHelper.getAllFinishedRequestCount();");
         if (o == null)
            return 0;
         return (Long) o;
      }catch (Exception e){
         try {
            Thread.sleep(3000);
         } catch (InterruptedException e1) {
            e1.printStackTrace();
         }
         return 0;
      }
   }

   @Override
   public void click(WebElement webElement) {
      try {
         ensureForNoActiveRequest();
         webElement.click();
      } catch (Exception e) {
         throw new SeleniumJUnitRuntimeException(e);
      }
   }

   @Override
   public void submit(WebElement webElement) {
      try {
         ensureForNoActiveRequest();
         webElement.submit();
      } catch (Exception e) {
         throw new SeleniumJUnitRuntimeException(e);
      }
   }

   @Override
   public void sendKeys(WebElement webElement, CharSequence... seq) {
      try {
         ensureForNoActiveRequest();
         webElement.sendKeys(seq);
      } catch (Exception e) {
         throw new SeleniumJUnitRuntimeException(e);
      }
   }


   @Override
   public List<SeleniumRequestDto> getAllFinishedRequests() {
      try {
         Object o = this.remoteWebDriver
                 .executeScript("return  JSON.stringify(window.__seleniumHelper.getAllFinishedRequests());");

         if (o == null)
            return Collections.EMPTY_LIST;

         String value = String.valueOf(o);
         TypeReference typeReference = new TypeReference<List<SeleniumRequestDto>>() {
         };
         try {
            List<SeleniumRequestDto> requestDtos = this.objectMapper.readValue(value, typeReference);
            return requestDtos;
         } catch (Exception e) {
            return Collections.EMPTY_LIST;
         }
      }catch (Exception e){
         return Collections.EMPTY_LIST;
      }
   }

   @Override
   public SeleniumRequestDto findFinishedRequestById(int id) {
      Object o = this.remoteWebDriver.executeScript("return window.__seleniumHelper.getAllFinishedRequestCount();");
      if (o == null)
         return null;
      try {
         return this.objectMapper.readValue(String.valueOf(o), SeleniumRequestDto.class);
      } catch (Exception e) {
         return null;
      }
   }

   private void ensureForNoActiveRequest() throws Exception {
      int tryCount = 0;
      int tryLimit = 0;

      while (tryCount <= tryLimit) {
         tryCount++;
         long activeRequestCount = this.getActiveRequestCount();
         if (activeRequestCount == 0) {
            break;

         }
         Thread.sleep(5000);
      }
   }
}
