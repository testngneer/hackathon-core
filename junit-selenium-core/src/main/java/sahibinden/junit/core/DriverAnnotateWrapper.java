package sahibinden.junit.core;

import sahibinden.junit.api.Driver;
import sahibinden.junit.api.DriverType;
import sahibinden.junit.factory.driver.WebDriverFactory;

public class DriverAnnotateWrapper {

   private Driver annotate;
   private DriverType driver;
   private final String mobileAgent;
   private final Class<? extends WebDriverFactory> driverFactory;

   public DriverAnnotateWrapper(Driver annotate, DriverType driver, String mobileAgent, Class<? extends WebDriverFactory> driverFactory) {
      this.annotate = annotate;
      this.driver = driver;
      this.mobileAgent = mobileAgent;
      this.driverFactory = driverFactory;
   }

   public void setDriver(DriverType driver) {
      this.driver = driver;
   }

   public DriverType getDriver() {
      return driver;
   }

   public String getMobileAgent() {
      return mobileAgent;
   }

   public Class<? extends WebDriverFactory> getDriverFactory() {
      return driverFactory;
   }

   public Driver getAnnotate() {
      return annotate;
   }

   public void setAnnotate(Driver annotate) {
      this.annotate = annotate;
   }

   public static DriverAnnotateWrapper createFromDriver(Driver driver) {
      return new DriverAnnotateWrapper(driver,driver.driver(), driver.mobileAgent(), driver.driverFactory());
   }
}
