package sahibinden.junit.core;

import sahibinden.junit.SeleniumSession;

import java.util.concurrent.ThreadFactory;

public class SeleniumThreadFactory implements ThreadFactory {

   private final SeleniumSession seleniumSession;

   public SeleniumThreadFactory(SeleniumSession seleniumSession) {
      this.seleniumSession = seleniumSession;
   }

   @Override
   public Thread newThread(Runnable runnable) {
      Thread thread = new Thread(runnable);
      thread.setDaemon(true);
      return thread;
   }

}
