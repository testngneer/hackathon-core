(function (window) {
    var seleniumHelper = new SeleniumHelper();
    var __originOpenMethod = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function () {
        var id = seleniumHelper.nextIdIdentifier();
        this.id = id;
        seleniumHelper.activeRequests[id] = this;
        console.log('------');
        console.log('start', seleniumHelper);
        this.addEventListener('load', function () {
            console.dir(this);
            seleniumHelper.finishedRequests.push({
                id: this.id,
                response: this.response,
                responseText: this.responseText,
                status: this.status,
                responseURL: this.responseURL
            });
            delete seleniumHelper.activeRequests[this.id];
            console.log('end', seleniumHelper);
            console.log('------');
        });
        __originOpenMethod.apply(this, arguments);
    };
    window.__seleniumHelper = seleniumHelper;
})(window);


function SeleniumHelper() {
    this.activeRequests = {};
    this.finishedRequests = [];
    this.currentIdIdentifier = 0;
}

SeleniumHelper.prototype.getActiveRequests = function () {
    var self = this;
    var arr = [];
    for (var i in self.activeRequests) {
        arr.push(self.activeRequests[i])
    }
    return arr;
};

SeleniumHelper.prototype.getActiveRequestCount = function () {
    var self = this;
    return self.getActiveRequests().length;
};

SeleniumHelper.prototype.getAllFinishedRequests = function () {
    return this.finishedRequests;
};

SeleniumHelper.prototype.findFinishedRequestsById = function (id) {
    var self = this;
    var filtered = self.finishedRequests.filter(function (value) {
        return value.id == id
    });
    return filtered.length === 0 ? null : filtered[0];
};

SeleniumHelper.prototype.getAllFinishedRequestCount = function () {
    return this.finishedRequests.length;
};

SeleniumHelper.prototype.nextIdIdentifier = function () {
    var self = this;
    return self.currentIdIdentifier++;
};
