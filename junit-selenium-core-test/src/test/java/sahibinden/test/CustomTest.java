package sahibinden.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import sahibinden.junit.SeleniumExtension;
import sahibinden.junit.api.Driver;
import sahibinden.junit.api.DriverType;
import sahibinden.junit.api.SeleniumTestApplication;
import sahibinden.junit.core.Agents;


@SeleniumTestApplication
@ExtendWith(SeleniumExtension.class)
public class CustomTest extends AbstractTest {

   @Test
   @Driver(driver = DriverType.CHROME, mobileAgent = Agents.Android.ANDROID_4_2_1, width = 320, heigth = 640, pixelRatio = 3.0)
   @DisplayName("test")
   public void test() {

      browser.get("https://www.agoda.com");
      browser.initSeleniumHelper();
      browser.getAllFinishedRequests();

      System.out.println("");
   }
}
